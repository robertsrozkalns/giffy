import logging

from tornado import gen
import momoko

class UserDAO(object):
    def __init__(self, db):
        self.db = db

    @gen.coroutine
    def get(self, id=0):
        sql = """
            SELECT id, username, email
            FROM users
            WHERE id=%s
        """
        cursor = yield self.db.execute(sql, (id,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        if len(result) == 0:
            result = None
        else:
            result = result[0]
        raise gen.Return(result)

    @gen.coroutine
    def get_by_email(self, email):
        sql = """
            SELECT id, username, email, password
            FROM users
            WHERE email=%s
        """
        cursor = yield self.db.execute(sql, (email,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        if len(result) == 0:
            result = None
        else:
            result = result[0]
        raise gen.Return(result)

    @gen.coroutine
    def get_by_username(self, username):
        sql = """
            SELECT id, username, email, password
            FROM users
            WHERE username=%s
        """
        cursor = yield self.db.execute(sql, (username,))
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        if len(result) == 0:
            result = None
        else:
            result = result[0]
        raise gen.Return(result)

    @gen.coroutine
    def get_list(self):
        sql = """
            SELECT id, username, email
            FROM users
        """
        cursor = yield self.db.execute(sql)
        desc = cursor.description
        result = [dict(zip([col[0] for col in desc], row))
                         for row in cursor.fetchall()]
        cursor.close()
        raise gen.Return(result)

    @gen.coroutine
    def create(self, username, email, password):
        sql = """
            INSERT INTO users (username, email, password)
            VALUES (%s, %s, %s  )
            RETURNING *;
        """
        cursor = yield self.db.execute(sql, (username, email, password))
        row_id = cursor.fetchone()[0]
        cursor.close()
        raise gen.Return(row_id)


    @gen.coroutine
    def update(self, id, data={}):
        fields = ''
        for key in data.keys():
            fields += '{0}=%s,'.format(key)

        sql = """
            UPDATE users
            SET {0}
            WHERE id=%s
        """.format(fields[0:-1])
        params = list(data.values())
        params.append(id)
        cursor = yield self.db.execute(sql, params)
        cursor.close()
        raise gen.Return(cursor)

    @gen.coroutine
    def delete(self, id):
        sql = """
            DELETE
            FROM users
            WHERE id=%s
        """
        cursor = yield self.db.execute(sql, (id,))
        cursor.close()
        raise gen.Return(cursor)
