#!/usr/bin/env bash
# Run giffy development server in Vagrant box.
#
# Example usage:
#   $ runserver.sh

set -e

main() {
  vagrant ssh -c "/vagrant/env/bin/python /vagrant/giffy.py --config=local"
}

main
